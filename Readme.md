
# Introduction

This is a test project created for Together Digital Company using HTML, CSS & JavaScript.
The objective of this project is to convert the given [design](https://projects.invisionapp.com/share/DQZ9BVX9CJF#/screens) into a webpage. The webpage is made responsive for mobile, tablets & desktop resolution.

# Requirements
 - Modern web browser (Google Chrome Recommended)
 
# Instructions
 - Unzip the file **TG_FRONTEND_TEST**.zip
 - Open the index.html file from the folder **TG_FRONTEND_TEST** in the browser of your choice.
# Implementation

The main aim of this project was to make the provided web page design responsive for desktop, tablet and mobile resolutions taking loading speed into cosiderations.  While there are quite a few libraries avaliable for grid layouts and other features, they come with a lot of unnecessary lines of code which significantly degrades the performance of the webpage. 

Instead of using libraries, custom css classes are written. For making the page respsonsive CSS's Flex property is used extensively througout the webpage. JQuery custom code is used for building the testimonial section of the webpage. 

In the provided design **font-family:poppins** is used with various **font-weight**s and they are implemented using Google Fonts.
 According to [Google Lighthouse](https://web.dev/performance-scoring/) ,  score between 90-100 is consederd as good. The scored achieved for this webpage is **93**.
![LightHouse Score](assets/score.png)


# Assumptions

 - Font Size : In the provided design font-sizes are not mentioned therefore, font-sizes have tested manually and assumed.
 - Font Color : All the font colors are inspected from the provided design and assumed.
 - Dimensions: All the dimensions on the webpage are assumed relative to the the design provided (Margins,paddings, image widths, button sizes etc.)
 - News Sections: On news section it is assumed that on selecting the specific menu, particular sections will be opened is assumed.

# FAQs
1. On what browsers this page is tested?
**Ans** : This webpage is tested on Google Chrome, Mozilla Firefox, Microsoft Edge, Duck Duck Go & Brave

2. On what Resolutions this pages is tested?
**Ans**: This webpage is tested for Desktop, Tablet & Mobile resolution.

3. What are the technolgies and libraries are used in this project?
**Ans**: HTML5, CSS3 , JavaScript is used in this project & jQuery library is used.


# Known Issues

 - This webpage dosent perform as expected on internet explorer browser
 
# Future Scope
 - Automated functional testing can be performed using testing frameworks such as Nightwatche, Testcafe etc.
 - Performance of webpage can be tested on more wide range of browsers.
 - All the js and css files can be compressed and minified to improve the performance of the webapge.

# Conclusion
While working on this project I have understood the importance & how to play with the **"em"** units.
I also learned to implement the custom slider.
Overall it was a good learning experience.
