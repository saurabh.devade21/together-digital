$(document).ready(function(){

    /**
     * function for showing the selected heading in news sections
     * it will add class selected-text to clicked item and after that it will find other sibling item and 
     * will remove class selected-text from all other siblings
     * 
     */
    $(".news-section-header-items").click(function(){
        $(this).addClass("selected-text").siblings("div").removeClass("selected-text");
    })


    /**
     * 
     * This function takes care of hiding and showing the Q&A sections
     * simple jquery toggle is used
     * and src attribute of image is used to change the url of image
     * open to minus and minus to open
     */
    $(".open-close-button").click(function(){
        $(this).parent().siblings().toggle('slow');
        if($(this).children().attr('src')==="assets/open.svg"){
            $(this).children().attr('src', 'assets/close.svg');
        }else{
            $(this).children().attr('src', 'assets/open.svg');
        }
        

    })

    /**
     * This function is for hiding and displaying mobile dropdown for news section
     * 
     */
    var activeElement=null;
    
    $(".mobile-news-section span").click(function(){
        $(activeElement).siblings('li').toggle('slow');
        
    })
    $(".mobile-news-section li").click(function(){
        activeElement=this;
        $(this).show('slow').siblings('li').hide('slow');
    })
    $('.mobile-news-section li:first').click();


    /**
     * To expand the floating buttons on hovering
     * 
     */
    $(".toolbar div").hover(
        function () {
          $(this).children("span").show('slow');
        },
        function () {
            $(this).children("span").hide('slow');
        }
      );

})