$(document).ready(function () {
  $("#slider-item-1").show();
  var sliderItmesSize = $(".slider-container div").length;
  var currentSlider = 1;
  $("#indicator-" + currentSlider + "").addClass("selected-indicator");

/**
 * This function is responisble for sliding the testimonal towards left
 * it hides or shows the testimonals based current active element id
 * slide left = decrement & slide right = increment the id
 * 
 */
  $(".left-arrow").click(function () {
    if (currentSlider == 1) {
      currentSlider = sliderItmesSize;
    } else {
      currentSlider--;
    }
    console.log(currentSlider);
    $("#slider-item-" + currentSlider + "")
      .show("slow")
      .siblings("div")
      .hide("slow");
    $("#indicator-" + currentSlider + "")
      .addClass("selected-indicator")
      .siblings("span")
      .removeClass("selected-indicator");
  });

  /**
 * This function is responisble for sliding the testimonal towards right
 * it hides or shows the testimonals based current active element id
 * slide left = decrement & slide right = increment the id 
 * once we get the decremented and incremented id it hide shows elemnet of that id
 * 
 */
  $(".right-arrow").click(function () {
    if (currentSlider == sliderItmesSize) {
      currentSlider = 1;
    } else {
      currentSlider++;
    }

    $("#slider-item-" + currentSlider + "")
      .show("slow")
      .siblings("div")
      .hide("slow");
    $("#indicator-" + currentSlider + "")
      .addClass("selected-indicator")
      .siblings("span")
      .removeClass("selected-indicator");
  });

  /**
   * function to trigger the click event on right arrow for autoslide
   * 
   */
  function autoSlide() {
    $(".right-arrow").click();
  }

  /**
   * 
   * Using setInterval method autoslide function is called every 5 second
   */
  var autoSlideVar = setInterval(autoSlide, 5000);

  /**
   * This function stops the autoslide once user hover on it
   * and after moving the cursor out of slider it will start autosliding
   * 
   */
  $(".slider-container").hover(
    function () {
      clearInterval(autoSlideVar);
    },
    function () {
      autoSlideVar = setInterval(autoSlide, 5000);
    }
  );
});
